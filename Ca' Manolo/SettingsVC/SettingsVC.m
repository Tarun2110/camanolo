//
//  SettingsVC.m
//  BlueAlertGPS
//
//  Created by Rakesh Kumar on 25/05/16.
//  Copyright © 2016 Rakesh Kumar. All rights reserved.
//

#import "SettingsVC.h"

@implementation SettingsVC

@synthesize htmlFile;




- (IBAction)TappedAction_MenuButton:(id)sender {

    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [NSHTTPCookieStorage sharedHTTPCookieStorage].cookieAcceptPolicy =
    NSHTTPCookieAcceptPolicyAlways;
    
    NSLog(@"%@",htmlFile);
    
    NSString *Requestload = [NSString stringWithFormat:@"https://www.tumenuweb-login.com/mobile/menu?company_uid=44a996b0-6d4d-40c0-bc7e-996e4986c8bb&restaurant_uid=%@",htmlFile];
    
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:Requestload]];
    
    //Load the request in the UIWebView.
    [_myWebView loadRequest:requestObj];
    
    
    _myWebView.userInteractionEnabled = YES;
    _myWebView.opaque = NO;
    _myWebView.backgroundColor = [UIColor clearColor];
}

@end
