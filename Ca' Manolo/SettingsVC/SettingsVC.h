//
//  SettingsVC.h
//  BlueAlertGPS
//
//  Created by Rakesh Kumar on 25/05/16.
//  Copyright © 2016 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsVC : UIViewController
@property (strong,nonatomic) NSString *htmlFile;
@property (strong, nonatomic) IBOutlet UIWebView *myWebView;

@end
