//
//  HomeVC.h
//  BlueAlertGPS
//
//  Created by Rakesh Kumar on 25/05/16.
//  Copyright © 2016 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>


@interface HomeVC : UIViewController<CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource,MKMapViewDelegate>
{
    IBOutlet UIWebView *myWeb;
    IBOutlet UISegmentedControl *SegmentControl;
    IBOutlet UIView *ListView;
    IBOutlet UIView *mapView;
    IBOutlet UITableView *listtableView;
    
    NSString *UserLat;
    NSString *UserLong;
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    NSString *currentLat;
    NSString *currentLong;
    MKCoordinateRegion region;
    UILabel *lblName;
    
}

@property (nonatomic, strong) IBOutlet MKMapView *map;
- (NSComparisonResult)reverseCompare:(NSNumber *)otherNumber;
@end
