//
//  HomeVC.m
//  BlueAlertGPS
//
//  Created by Rakesh Kumar on 25/05/16.
//  Copyright © 2016 Rakesh Kumar. All rights reserved.
//

#import "HomeVC.h"
#import "CommentCustomCell.h"
#import "SettingsVC.h"
#import "ZSAnnotation.h"
#import "pinObject.h"


@implementation HomeVC
{
    NSMutableArray *RestNameArray;
    NSMutableArray *AddressArray;
    NSMutableArray *HtmlStatus;
    NSMutableArray *ListingArray;
    NSMutableArray *ComingStatus;
    NSMutableArray *PhoneNumberArray;
    NSMutableArray *latArray;
    NSMutableArray *longArray;
    NSMutableArray *distanceArray;
    NSMutableArray *mappinsArray;
    BOOL mapLoaded;
    int LocationEnabled;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [SegmentControl addTarget:self action:@selector(segmentAction:) forControlEvents: UIControlEventValueChanged];
    
    SegmentControl.selectedSegmentIndex = 0;
    
    [mapView setHidden:NO];
    [ListView setHidden:YES];
    
    self.map.showsUserLocation = TRUE;
    self.map.delegate = self;

    
}

- (IBAction)Back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    mapLoaded = FALSE;
}

-(void) viewDidAppear:(BOOL)animated
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9)
    {
        //locationManager.allowsBackgroundLocationUpdates = YES;
    }
    locationManager = [[CLLocationManager alloc] init];
    [locationManager requestWhenInUseAuthorization];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.headingFilter = 1;
    locationManager.delegate = self;
    
    if([CLLocationManager locationServicesEnabled])
    {
        NSLog(@"Location Services Enabled");
    }
    else
    {
       
    }
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status)
    {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            float latti = [@"28.1106261" floatValue];
            float Longi = [@"-15.4307394999999" floatValue];
            
            CLLocationCoordinate2D coords = (CLLocationCoordinate2D){ latti, Longi };
            region = MKCoordinateRegionMakeWithDistance(coords, 12000, 9000);
           
            [self.map setRegion:region animated:YES];
            [self LoadData];
            LocationEnabled = 0;
            
        }
        break;
        default:
        {
            [locationManager startUpdatingLocation];
             mapLoaded = FALSE;
            self.map.showsUserLocation = TRUE;
            self.map.delegate = self;

            LocationEnabled = 1;
            
            [self LoadData];
        }
        break;
    }
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
   
    currentLat =[NSString stringWithFormat:@"%f",location.coordinate.latitude];
    currentLong = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    
    if(mapLoaded == FALSE)
    {
        currentLat = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
        currentLong = [NSString stringWithFormat:@"%f",location.coordinate.longitude];;
        
        CLLocationCoordinate2D coords = (CLLocationCoordinate2D){ [currentLat floatValue] , [currentLong floatValue] };
        region = MKCoordinateRegionMakeWithDistance(coords, 1000, 1000);
        
        [self.map setRegion:region animated:YES];
        mapLoaded = TRUE;
    }
}


#pragma mark  TableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ListingArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CommentCell";
    CommentCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[CommentCustomCell alloc] init];
    }
    cell = [[[NSBundle mainBundle ] loadNibNamed:@"CommentCustomCell" owner:self options:nil] objectAtIndex:0];
    
    if ([[[ListingArray valueForKey:@"Status"] objectAtIndex:indexPath.row] isEqualToString:@"0"]) // Coming Soon
    {
        [cell.comingsoonImage setHidden:NO];
        cell.Resturantbutton.userInteractionEnabled = NO;
        cell.Resturantbutton.alpha = 0.2f;
    }
    else
    {
        [cell.comingsoonImage setHidden:YES];
    }
   
    cell.phoneNumberLabel.text = [[ListingArray valueForKey:@"phone"] objectAtIndex:indexPath.row];
    cell.addresslabel.text = [[ListingArray valueForKey:@"Address"] objectAtIndex:indexPath.row];
    cell.distanceLabel.text = [NSString stringWithFormat:@"%@ kms",[[ListingArray valueForKey:@"distance"] objectAtIndex:indexPath.row]];
    [cell.Resturantbutton setTitle:[[ListingArray valueForKey:@"Name"] objectAtIndex:indexPath.row] forState:UIControlStateNormal];
    [cell.Resturantbutton addTarget:self action:@selector(YesbuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    cell.Resturantbutton.tag = indexPath.row;

    return cell;
}

-(void)YesbuttonClicked:(UIButton*)sender
{
    UIButton *button=(UIButton *)sender;
    NSInteger tag = [button tag];
    
    SettingsVC *ObjSettingsVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SettingsVC"];
    ObjSettingsVC.htmlFile = [[ListingArray valueForKey:@"html"] objectAtIndex:tag] ;
    [self.navigationController pushViewController:ObjSettingsVC animated:YES];
}



-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return 60;
}


-(void)ShowPinsOnMap
{
    NSMutableArray * ann = [[NSMutableArray alloc] init];

    for (pinObject * p in mappinsArray)
    {
        ZSAnnotation *mapPin = [[ZSAnnotation alloc] init];
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(p.pinLat, p.pinLong);
        mapPin.coordinate = coordinate;
        mapPin.pin = p;
        [ann addObject:mapPin];
    }
    [self.map addAnnotations:ann];
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if(mapLoaded == FALSE)
    {
        float lat = [[NSString stringWithFormat:@"%@",currentLat] floatValue];
        float Long = [[NSString stringWithFormat:@"%@",currentLong] floatValue];
        
        CLLocationCoordinate2D coords = (CLLocationCoordinate2D){ lat , Long };
        region = MKCoordinateRegionMakeWithDistance(coords, 1000, 1000);
       
        [self.map setRegion:region animated:YES];
        mapLoaded = TRUE;
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    if(![annotation isKindOfClass:[ZSAnnotation class]])
        return nil;
    
    MKAnnotationView *pinView = nil;
    
    ZSAnnotation *a = (ZSAnnotation *)annotation;
    static NSString *defaultPinID = @"StandardIdentifier";
    
    pinView = (MKAnnotationView *)[self.map dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    if (pinView == nil)
    {
        pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
    }
    if([a.pin.pinStatus isEqualToString:@"1"])
    {
        pinView.image = [UIImage imageNamed:@"pin_active"];
    }
    else
    {
        pinView.image = [UIImage imageNamed:@"pin_normal"];
    }

    a.title = a.pin.pinTitle;
    a.subtitle = a.pin.pinDesc;

    pinView.canShowCallout = YES;
    pinView.userInteractionEnabled = YES;
    return pinView;
}



- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    [self.map setCenterCoordinate:view.annotation.coordinate animated:YES];
}

- (void)segmentAction:(UISegmentedControl *)segment
{
    switch (segment.selectedSegmentIndex)
    {
        case 0:
            [mapView setHidden:NO];
            [ListView setHidden:YES];
            [self.map setRegion:region animated:YES];

            break;
        case 1:
            [mapView setHidden:YES];
            [ListView setHidden:NO];

            [self LoadData];
            [self ShortArrayAsPerDistance];

            break;
        default:
            break;
    }
}


-(NSString *) UpdateLatLong :(NSString *)lat long:(NSString *)lon
{
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[lon doubleValue]];
    
    currentLocation = [[CLLocation alloc] initWithLatitude:[currentLat doubleValue] longitude:[currentLong doubleValue]];

    float Meters = [location1 distanceFromLocation:currentLocation];
    CLLocationDistance kilometers = Meters / 1000.0;
    NSString *kms = [NSString stringWithFormat:@"%f",kilometers];
    NSArray *tempArray = [kms componentsSeparatedByString:@"."];
    kms = [tempArray objectAtIndex:0];
    
    return kms;
}



-(void) ShortArrayAsPerDistance
{
    NSArray *sortedArray = [ListingArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        if ([[obj1 objectForKey:@"distance"] floatValue] > [[obj2 objectForKey:@"distance"] floatValue])
            return NSOrderedDescending;
        else if ([[obj1 objectForKey:@"distance"] floatValue] < [[obj2 objectForKey:@"distance"] floatValue])
            return NSOrderedAscending;
        return NSOrderedSame;
    }];
    
    ListingArray = [sortedArray mutableCopy];
    
    listtableView.delegate = self;
    listtableView.dataSource = self;
    [listtableView reloadData];
    
    if (LocationEnabled == 0)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Background Location Access Disabled"
                                     message:@"Your current location will be displayed on the map and used for directions, and estimated travel times."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //Handle your yes please button action here
                                    }];
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }

}

-(void)viewDidDisappear:(BOOL)animated
{
    self.map = nil;
}

-(void) LoadData
{
    mappinsArray = [NSMutableArray new];
    distanceArray = [NSMutableArray new];
    ListingArray = [NSMutableArray new];
    
    self.map.showsUserLocation = TRUE;
    self.map.delegate = self;
    
    RestNameArray = [[NSMutableArray alloc] initWithObjects:@"Ca' Manolo Schamann",@"Ca' Manolo 7 Palmas",@"Ca' Manolo Tamaraceite",@"Ca' Manolo Puerto Canteras",@"Ca' Manolo San José",@"Ca' Manolo Guanarteme",@"Ca' Manolo Telde", @"Ca' Manolo Arucas", @"Ca' Manolo Vecindario",@"Ca' Manolo El Carrizal", @"Ca' Manolo Gáldar", @"Ca' Manolo San Fernando", nil];
    
    AddressArray = [[NSMutableArray alloc] initWithObjects:@"Calle Don Pedro Infinito, 18,Las Palmas ",@"Av Pintor Felo Monzón, 28,Las Palmas ",@"Carr. a Tamaraceite, 97,Las Palmas",@"Calle Albareda, 52,Las Palmas",@"Paseo de San Jose, 110, Las Palmas ",@"Calle Gravina, 39,Las Palmas",@"Calle Rivero Bethencourt, 6, Telde", @"Paseo Poeta Pedro Lezcano, 14,Arucas", @"Av. de Canarias, 262,Vecindario",@"Calle Barcelona, 9,Carrizal", @"Calle Bentejuí, 4, Gáldar", @"Calle Alcalde Marcial Franco, 16,San Bartolomé de Tirajana", nil];
    
    HtmlStatus = [[NSMutableArray alloc] initWithObjects:@"3d9d462f-1e6a-465e-9872-bd1814a2ccc8",@"b022af32-00cf-4d73-80fd-bc52f5a70378",@"f15f731e-a628-418d-937e-9e689b86e946",@"9bd3958d-887a-4979-b717-20f4ab2c76cb",@"473964ed-61e0-4a7e-a3d0-4f359a8b5f35",@"67bbf9d2-397b-4b3a-99e6-8e72dacedaa5",@"fe39d640-0e89-463e-90ac-828c2f5e71b5",@"d13ab22f-dd97-4227-84f5-a85ef141f0fd",@"c418c743-4042-4f9a-8ec6-a83669f52580",@"86f375c5-87a0-4457-9ebd-605699835527",@"9000eb7c-0edd-41e5-8c83-ee204272695e",@"8a5782e4-031a-406d-99f8-136de1459f92", nil];
    
    ComingStatus = [[NSMutableArray alloc] initWithObjects:@"1",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"1",@"0", nil];
    
    PhoneNumberArray = [[NSMutableArray alloc] initWithObjects:@"928 206 411",@"928051719",@"928667728",@"928930666",@"928907219",@"928266778",@"928704500",@"828045258",@"928507054",@"928960042",@"928552763",@"828129436", nil];
    
    latArray = [[NSMutableArray alloc] initWithObjects:@"28.1106261",@"28.1043799",@"28.1144821",@"28.1463611",@"28.0936984",@"28.1334587",@"27.9954301",@"28.116891",@"27.8490453",@"27.9129500",@"28.1441364",@"27.7644356", nil];
    
    longArray = [[NSMutableArray alloc] initWithObjects:@"-15.4307394999999",@"-15.4530125",@"-15.4556076",@"-15.4296833333333",@"-15.4186568",@"-15.4378902",@"-15.4183332",@"-15.527538",@"-15.4424780",@"-15.4044228",@"-15.6552711",@"-15.5773306", nil];
    
    
    for (int i = 0 ; i != AddressArray.count ; i++)
    {
        
        [ListingArray addObject:
           @{
           @"Name" : RestNameArray[i]
           ,@"Address" : AddressArray[i]
           ,@"html" : HtmlStatus[i]
           ,@"Status" : ComingStatus[i]
           ,@"phone" : PhoneNumberArray[i]
           ,@"lat" : latArray[i]
           ,@"long" : longArray[i],
           @"distance" : [self UpdateLatLong:latArray[i] long:longArray[i]]
           }];
        
        pinObject * p = [pinObject new];
        p.pinLat =[[[ListingArray valueForKey:@"lat"] objectAtIndex:i]floatValue];
        p.pinLong = [[[ListingArray valueForKey:@"long"] objectAtIndex:i] floatValue];
        p.pinStatus = [[ListingArray valueForKey:@"Status"] objectAtIndex:i];
        p.pinTitle = [[ListingArray valueForKey:@"Name"] objectAtIndex:i];
        p.pinId = [[ListingArray valueForKey:@"html"] objectAtIndex:i];
        p.pinDesc = [[ListingArray valueForKey:@"Address"] objectAtIndex:i];
        
        [mappinsArray addObject:p];
    }
    
    
    NSLog(@"%@",ListingArray);
    
    [self ShowPinsOnMap];
  
    
 }


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
