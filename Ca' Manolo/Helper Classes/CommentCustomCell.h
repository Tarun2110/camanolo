//
//  TableCustomCell.h
//  PollingApp
//
//  Created by Rakesh Kumar on 04/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentCustomCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *addresslabel;

@property (strong, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (strong, nonatomic) IBOutlet UILabel *distanceLabel;
@property (strong, nonatomic) IBOutlet UIImageView *comingsoonImage;
@property (strong, nonatomic) IBOutlet UIButton *Resturantbutton;

@end
