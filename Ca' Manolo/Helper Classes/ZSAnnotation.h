//
//  EndAnnotation.h
//  Ride
//
//  Created by Nicholas Hubbard on 5/2/10.
//  Copyright (c) 2013 Zed Said Studio. All rights reserved.
//
#import "pinObject.h"
#import <Foundation/Foundation.h>
#import <MapKit/MKAnnotation.h>


@interface ZSAnnotation : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, retain) pinObject *pin;


@end
