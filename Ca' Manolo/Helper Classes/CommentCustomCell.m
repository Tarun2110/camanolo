//
//  TableCustomCell.m
//  PollingApp
//
//  Created by Rakesh Kumar on 04/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "CommentCustomCell.h"

@implementation CommentCustomCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code

    _Resturantbutton.layer.masksToBounds = YES;
    _Resturantbutton.layer.cornerRadius = _Resturantbutton.frame.size.height/2;
    _Resturantbutton.layer.borderWidth = 0.5;
    _Resturantbutton.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
