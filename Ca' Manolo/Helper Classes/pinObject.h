//
//  pinObject.h
//  Kheops
//
//  Created by Rakesh Kumar on 12/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface pinObject : NSObject
@property (nonatomic, strong) NSString * pinTitle, *pinDesc, *pinId;
@property NSString* pinStatus;
@property float pinLat, pinLong;
@end
